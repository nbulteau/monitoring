'use strict';

angular.module('monitoringApp')
    .controller('NavbarController', function ($scope, $location, $state, ENV) {
        $scope.$state = $state;
        $scope.inProduction = ENV === 'prod';
    });
