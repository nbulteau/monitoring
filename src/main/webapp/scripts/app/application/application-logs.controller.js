'use strict';

angular.module('monitoringApp')
    .controller('ApplicationLogsController', function ($scope, $stateParams, MonitoringService) {
        $scope.applicationId = $stateParams.id;

      $scope.refresh = function() {
        $scope.updatingLogs = true;
        MonitoringService.logs($scope.applicationId).then(function(response) {
          $scope.loggers = response;
          $scope.updatingLogs = false;
        }, function(response) {
          $scope.loggers = response.data;
          $scope.updatingLogs = false;
        });
      };

        $scope.changeLevel = function (name, level) {
            MonitoringService.changeLevel($scope.applicationId, {name: name, level: level}).then(function () {
                $scope.refresh();
            });
        };

      $scope.refresh();

    });
