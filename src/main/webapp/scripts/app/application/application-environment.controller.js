'use strict';

angular.module('monitoringApp').controller('ApplicationEnvironmentController', function($scope, $stateParams, $modal, MonitoringService) {
  $scope.applicationId = $stateParams.id;
  $scope.updating = true;
  
  $scope.keys = function(obj){
    return obj? Object.keys(obj) : [];
  }

  $scope.refresh = function() {
    $scope.updatingEnvironment = true;
    MonitoringService.environment($scope.applicationId).then(function(response) {
      $scope.systemProperties = response.systemProperties;
      $scope.systemEnvironment = response.systemEnvironment;
      
      $scope.updating = false;
    });
  };

  $scope.refresh();

});
