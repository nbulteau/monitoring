'use strict';

angular.module('monitoringApp')
    .controller('ApplicationMetricsController', function ($scope, $stateParams, $modal, MonitoringService) {
        $scope.metrics = {};
        $scope.applicationId = $stateParams.id;
        
        $scope.updatingMetrics = true;

        $scope.refresh = function() {
          $scope.updatingMetrics = true;
          MonitoringService.metrics($scope.applicationId).then(function(response) {
            $scope.metrics = response;
            $scope.updatingMetrics = false;
          }, function(response) {
            $scope.metrics = response.data;
            $scope.updatingMetrics = false;
          });
        };
        
        $scope.$watch('metrics', function (newValue) {
            $scope.poolsList = [];
            $scope.servicesStats = {};
            $scope.cachesStats = {};
            angular.forEach(newValue.timers, function (value, key) {
                if (key.indexOf('web.rest') !== -1 || key.indexOf('service') !== -1) {
                    $scope.servicesStats[key] = value;
                }
                if (key.indexOf('net.sf.ehcache.Cache') !== -1) {
                    // remove gets or puts
                    var index = key.lastIndexOf('.');
                    var newKey = key.substr(0, index);

                    // Keep the name of the domain
                    index = newKey.lastIndexOf('.');
                    $scope.cachesStats[newKey] = {
                        'name': newKey.substr(index + 1),
                        'value': value
                    };
                }
                if (key.indexOf('pool.Wait') !== -1) {
                    $scope.poolsList.push(key.replace(".pool.Wait", ""));
                }
            });
        });

        $scope.refresh();

        $scope.refreshThreadDumpData = function() {
            MonitoringService.threadDump($scope.applicationId).then(function(data) {
                var modalInstance = $modal.open({
                    templateUrl: 'scripts/app/application/metrics.modal.html',
                    controller: 'MetricsModalController',
                    size: 'lg',
                    resolve: {
                        threadDump: function() {
                              if(data.content) {
                                 return data.content;
                              } else {
                                 return data;
                              }
                        }
                    }
                });
            });
        };
    });
