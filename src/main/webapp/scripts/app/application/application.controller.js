'use strict';

angular.module('monitoringApp').controller('ApplicationController', function($scope, $state, $modal, $interval, Application) {

  $scope.sortType     = 'catalinaBase'; // set the default sort type
  $scope.sortReverse  = false;     // set the default sort order
  $scope.search       = '';        // set the default search/filter term

  $scope.applications = [];
  $scope.loadAll = function() {
    Application.query(function(result) {
      $scope.applications = result;
    });
  };
  $scope.loadAll();

  $scope.getLabelClass = function(statusState) {
    if (statusState === 'UP') {
      return 'label-success';
    }
    if (statusState === 'OFFLINE') {
      return 'label-default';
    }
    else if (statusState === 'UNKNOWN') {
      return 'label-info';
    }
    else {
      return 'label-danger';
    }
  };

  $scope.refresh = function() {
    $scope.loadAll();
    $scope.clear();
  };

  $scope.intervalPromise = $interval(function(){
    $scope.refresh();
  }, 10000);

  $scope.clear = function() {
    $scope.application = {
      url : null,
      name : null,
      id : null
    };
  };
});
