'use strict';

angular.module('monitoringApp')
    .factory('Application', function ($resource) {
        return $resource('api/applications/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    })
    .factory('MonitoringService', function($rootScope, $http) {
      return {
        metrics : function(id) {
          return $http.get('api/applications/' + id + '/metrics').then(function(response) {
            return response.data;
          });
        },
        checkHealth : function(id) {
          return $http.get('api/applications/' + id + '/health').then(function(response) {
            return response.data;
          });
        },
        environment : function(id) {
          return $http.get('api/applications/' + id + '/env').then(function(response) {
            return response.data;
          });
        },
        beans : function(id) {
          return $http.get('api/applications/' + id + '/beans').then(function(response) {
            return response.data;
          });
        },
        logs : function(id) {
          return $http.get('api/applications/' + id + '/logs').then(function(response) {
            return response.data;
          });
        },
        changeLevel : function(id, data) {
          return $http.put('api/applications/' + id + '/logs', data).then(function(response) {
            return response.data;
          });
        },
        threadDump : function(id) {
          return $http.get('api/applications/' + id + '/dump').then(function(response) {
            return response.data;
          });
        }
      };
    });
