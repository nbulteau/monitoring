'use strict';

angular.module('monitoringApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('application', {
                parent: 'site',
                url: '/applications',
                data: {
                    pageTitle: 'Applications'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/application/applications.html',
                        controller: 'ApplicationController'
                    }
                },
                resolve: {
                }
            })
            .state('application.health', {
                parent: 'site',
                url: '/application/health/{id}',
                data: {
                    pageTitle: 'Health'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/application/application-health.html',
                        controller: 'ApplicationHealthController'
                    }
                },
                resolve: {
                }
            })
            .state('application.metrics', {
              parent: 'site',
              url: '/application/metrics/{id}',
              data: {
                  pageTitle: 'Metrics'
              },
              views: {
                  'content@': {
                      templateUrl: 'scripts/app/application/application-metrics.html',
                      controller: 'ApplicationMetricsController'
                  }
              },
              resolve: {
              }
          })
          .state('application.environment', {
            parent: 'site',
            url: '/application/environment/{id}',
            data: {
                pageTitle: 'Environment'
            },
            views: {
                'content@': {
                    templateUrl: 'scripts/app/application/application-environment.html',
                    controller: 'ApplicationEnvironmentController'
                }
            },
            resolve: {
            }
        })
        .state('application.beans', {
          parent: 'site',
          url: '/application/beans/{id}',
          data: {
              pageTitle: 'Beans'
          },
          views: {
              'content@': {
                  templateUrl: 'scripts/app/application/application-beans.html',
                  controller: 'ApplicationBeansController'
              }
          },
          resolve: {
          }
      })
      .state('application.logs', {
        parent: 'site',
        url: '/application/logs/{id}',
        data: {
            pageTitle: 'Beans'
        },
        views: {
            'content@': {
                templateUrl: 'scripts/app/application/application-logs.html',
                controller: 'ApplicationLogsController'
            }
        },
        resolve: {
        }
    })
    });
