'use strict';

angular.module('monitoringApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('developpement', {
                parent: 'site',
                url: '/developpement',
                data: {
                    pageTitle: 'Applications'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/developpement/developpement.html',
                        controller: 'DeveloppementController'
                    }
                },
                resolve: {
                }
            })
        .state('developpement.beans', {
          parent: 'site',
          url: '/developpement/beans/{id}',
          data: {
              pageTitle: 'Beans'
          },
          views: {
              'content@': {
                  templateUrl: 'scripts/app/developpement/developpement-beans.html',
                  controller: 'DeveloppementBeansController'
              }
          },
          resolve: {
          }
      })
    });
