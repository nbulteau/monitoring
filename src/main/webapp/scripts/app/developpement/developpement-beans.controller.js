'use strict';

angular.module('monitoringApp').controller('DeveloppementBeansController', function($scope, $stateParams, $modal, MonitoringService) {
  $scope.applicationId = $stateParams.id;
  $scope.updating = true;

  $scope.keys = function(obj){
    return obj? Object.keys(obj) : [];
  }

  $scope.refresh = function() {
    $scope.updatingEnvironment = true;
    MonitoringService.beans($scope.applicationId).then(function(response) {
      if(response.content) {
         $scope.beans = response.content[0].beans;
      } else {
         $scope.beans = response[0].beans;
      }

      $scope.updating = false;
    });
  };

  $scope.refresh();

});
