package fr.nbu.monitoring.domain;

import java.io.Serializable;
import java.util.Map;

/**
 * Represents a certain status a certain time.
 *
 */
public class StatusInfo implements Serializable {

  private static final long serialVersionUID = 1L;

  private final String status;

  private final Map<String, Object> details;

  private final long timestamp;

  protected StatusInfo(final String status, final long timestamp, final Map<String, Object> details) {
    this.status = status.toUpperCase();
    this.timestamp = timestamp;
    this.details = details;
  }

  public static StatusInfo valueOf(final String statusCode, final Map<String, Object> details) {
    return new StatusInfo(statusCode, System.currentTimeMillis(), details);
  }

  public static StatusInfo ofUnknown() {
    return valueOf("UNKNOWN", null);
  }

  public static StatusInfo ofUp(final Map<String, Object> details) {
    return valueOf("UP", details);
  }

  public static StatusInfo ofDown(final Map<String, Object> details) {
    return valueOf("DOWN", details);
  }

  public static StatusInfo ofOffline(final Map<String, Object> details) {
    return valueOf("OFFLINE", details);
  }

  public String getStatus() {
    return status;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public Map<String, Object> getDetails() {
    return details;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((status == null) ? 0 : status.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final StatusInfo other = (StatusInfo) obj;
    if (status == null) {
      if (other.status != null) {
        return false;
      }
    }
    else if (!status.equals(other.status)) {
      return false;
    }
    return true;
  }

}