package fr.nbu.monitoring.domain;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/**
 * A Application.
 */
public class Application implements Serializable {

  /**
   * Variable membre <code>serialVersionUID</code>
   */
  private static final long serialVersionUID = 1L;

  private final String id;

  private String name;

  private String rootUrl;

  private String catalinaBase;

  private List<String> cookies;

  private boolean metrics = false;

  private boolean environment = false;

  private boolean dump = false;

  private boolean beans = false;

  private boolean logs = false;

  private StatusInfo statusInfo = StatusInfo.ofUnknown();

  public Application() {
    super();
    this.id = UUID.randomUUID().toString();
  }

  public Application(final String name, final String rootUrl) {
    this();
    this.name = name;
    this.rootUrl = rootUrl;
  }

  public Application(final String id, final String name, final String rootUrl, final StatusInfo statusInfo) {
    super();
    this.id = id;
    this.name = name;
    this.rootUrl = rootUrl;
    this.statusInfo = statusInfo;
  }

  public Application(final Application application, final StatusInfo statusInfo) {
    this(application.getId(), application.getName(), application.getRootUrl(), statusInfo);
    this.setMetrics(application.isMetrics());
    this.setEnvironment(application.isEnvironment());
    this.setDump(application.isDump());
    this.setBeans(application.isBeans());
    this.setLogs(application.isLogs());
    this.setCatalinaBase(application.getCatalinaBase());
    this.setCookies(application.getCookies());
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getRootUrl() {
    return rootUrl;
  }

  public StatusInfo getStatusInfo() {
    return statusInfo;
  }

  public boolean isMetrics() {
    return metrics;
  }

  public void setMetrics(final boolean metrics) {
    this.metrics = metrics;
  }

  public boolean isEnvironment() {
    return environment;
  }

  public void setEnvironment(final boolean environment) {
    this.environment = environment;
  }

  public boolean isDump() {
    return dump;
  }

  public void setDump(final boolean dump) {
    this.dump = dump;
  }

  public boolean isBeans() {
    return beans;
  }

  public void setBeans(final boolean beans) {
    this.beans = beans;
  }

  public boolean isLogs() {
    return logs;
  }

  public void setLogs(final boolean logs) {
    this.logs = logs;
  }

  public String getCatalinaBase() {
    return catalinaBase;
  }

  public void setCatalinaBase(final String catalinaBase) {
    this.catalinaBase = catalinaBase;
  }

  public void setStatusInfo(final StatusInfo statusInfo) {
    this.statusInfo = statusInfo;
  }

  public void setCookies(final List<String> cookies) {
    this.cookies = cookies;
  }

  public List<String> getCookies() {
    return this.cookies;
  }
}
