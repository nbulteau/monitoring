package fr.nbu.monitoring;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import fr.nbu.monitoring.config.Constants;
import fr.nbu.monitoring.config.MonitoringProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.MetricFilterAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.MetricRepositoryAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.core.env.SimpleCommandLinePropertySource;

@ComponentScan
@EnableAutoConfiguration(exclude = { MetricFilterAutoConfiguration.class, MetricRepositoryAutoConfiguration.class })
@EnableConfigurationProperties({ MonitoringProperties.class, LiquibaseProperties.class })
public class Monitoring {

  private static final Logger LOG = LoggerFactory.getLogger(Monitoring.class);

  @Inject
  private Environment env;

  /**
   * Initializes monitoring.
   * <p/>
   * Spring profiles can be configured with a program arguments --spring.profiles.active=your-active-profile
   * <p/>
   * <p>
   * You can find more information on how profiles work with JHipster on <a href="http://jhipster.github.io/profiles.html">http://jhipster.github.io/profiles.html</a>.
   * </p>
   */
  @PostConstruct
  public void initApplication() {
    if (env.getActiveProfiles().length == 0) {
      LOG.warn("No Spring profile configured, running with default configuration");
    }
    else {
      LOG.info("Running with Spring profile(s) : {}", Arrays.toString(env.getActiveProfiles()));
      final Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
      if (activeProfiles.contains(Constants.SPRING_PROFILE_DEVELOPMENT) && activeProfiles.contains(Constants.SPRING_PROFILE_PRODUCTION)) {
        LOG.error("You have misconfigured your application! " +
            "It should not run with both the 'dev' and 'prod' profiles at the same time.");
      }
      if (activeProfiles.contains(Constants.SPRING_PROFILE_PRODUCTION) && activeProfiles.contains(Constants.SPRING_PROFILE_FAST)) {
        LOG.error("You have misconfigured your application! " +
            "It should not run with both the 'prod' and 'fast' profiles at the same time.");
      }
    }

  }

  /**
   * Main method, used to run the application.
   */
  public static void main(final String[] args) throws UnknownHostException {
    final SpringApplication app = new SpringApplication(Monitoring.class);
    final SimpleCommandLinePropertySource source = new SimpleCommandLinePropertySource(args);
    addDefaultProfile(app, source);
    final Environment env = app.run(args).getEnvironment();
    LOG.info("Access URLs:\n----------------------------------------------------------\n\t" +
        "Local: \t\thttp://127.0.0.1:{}\n\t" +
        "External: \thttp://{}:{}\n----------------------------------------------------------",
        env.getProperty("server.port"),
        InetAddress.getLocalHost().getHostAddress(),
        env.getProperty("server.port"));
  }

  /**
   * If no profile has been configured, set by default the "dev" profile.
   */
  private static void addDefaultProfile(final SpringApplication app, final SimpleCommandLinePropertySource source) {
    if (!source.containsProperty("spring.profiles.active") &&
        !System.getenv().containsKey("SPRING_PROFILES_ACTIVE")) {

      app.setAdditionalProfiles(Constants.SPRING_PROFILE_DEVELOPMENT);
    }
  }
}
