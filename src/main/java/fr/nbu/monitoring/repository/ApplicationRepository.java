package fr.nbu.monitoring.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Repository;

import fr.nbu.monitoring.domain.Application;

@Repository
public class ApplicationRepository {

  private final ConcurrentHashMap<String, Application> map = new ConcurrentHashMap<>();

  public ApplicationRepository() {
    super();
  }

  public Application save(final Application app) {
    map.put(app.getId(), app);
    return app;
  }

  public Application findOne(final String id) {
    return map.get(id);
  }

  public boolean exist(final String name) {
    final Optional<Application> application = map.values().stream().filter(a -> name.equals(a.getName())).findAny();
    return application.isPresent();
  }

  public List<Application> findAll() {
    return new ArrayList<>(map.values());
  }

  public Application delete(final String id) {
    return map.remove(id);
  }

}
