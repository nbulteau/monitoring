package fr.nbu.monitoring.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Monitoring properties.
 *
 * <p>
 *     Properties are configured in the application.yml file.
 * </p>
 */
@ConfigurationProperties(prefix = "monitoring", ignoreUnknownFields = false)
public class MonitoringProperties {

  private final Swagger swagger = new Swagger();

  public Swagger getSwagger() {
    return swagger;
  }

  public static class Swagger {

    private String title = "monitoring API";

    private String description = "monitoring API documentation";

    private String version = "0.0.1";

    private String termsOfServiceUrl;

    private String contact;

    private String license;

    private String licenseUrl;

    public String getTitle() {
      return title;
    }

    public void setTitle(final String title) {
      this.title = title;
    }

    public String getDescription() {
      return description;
    }

    public void setDescription(final String description) {
      this.description = description;
    }

    public String getVersion() {
      return version;
    }

    public void setVersion(final String version) {
      this.version = version;
    }

    public String getTermsOfServiceUrl() {
      return termsOfServiceUrl;
    }

    public void setTermsOfServiceUrl(final String termsOfServiceUrl) {
      this.termsOfServiceUrl = termsOfServiceUrl;
    }

    public String getContact() {
      return contact;
    }

    public void setContact(final String contact) {
      this.contact = contact;
    }

    public String getLicense() {
      return license;
    }

    public void setLicense(final String license) {
      this.license = license;
    }

    public String getLicenseUrl() {
      return licenseUrl;
    }

    public void setLicenseUrl(final String licenseUrl) {
      this.licenseUrl = licenseUrl;
    }
  }
}
