package fr.nbu.monitoring.config.apidoc;

import static springfox.documentation.builders.PathSelectors.*;

import java.util.Date;

import fr.nbu.monitoring.config.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;

import fr.nbu.monitoring.config.MonitoringProperties;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Springfox Swagger configuration.
 *
 * Warning! When having a lot of REST endpoints, Springfox can become a performance issue. In that
 * case, you can use a specific Spring profile for this class, so that only front-end developers
 * have access to the Swagger view.
 */
@Configuration
@EnableSwagger2
@Profile("!" + Constants.SPRING_PROFILE_PRODUCTION)
public class SwaggerConfiguration {

  private final Logger log = LoggerFactory.getLogger(SwaggerConfiguration.class);

  public static final String DEFAULT_INCLUDE_PATTERN = "/api/.*";

  /**
   * Swagger Springfox configuration.
   */
  @Bean
  public Docket swaggerSpringfoxDocket(final MonitoringProperties monitoringProperties) {
    log.debug("Starting Swagger");
    final StopWatch watch = new StopWatch();
    watch.start();
    final ApiInfo apiInfo = new ApiInfo(
        monitoringProperties.getSwagger().getTitle(),
        monitoringProperties.getSwagger().getDescription(),
        monitoringProperties.getSwagger().getVersion(),
        monitoringProperties.getSwagger().getTermsOfServiceUrl(),
        monitoringProperties.getSwagger().getContact(),
        monitoringProperties.getSwagger().getLicense(),
        monitoringProperties.getSwagger().getLicenseUrl());

    final Docket docket = new Docket(DocumentationType.SWAGGER_2)
        .apiInfo(apiInfo)
        .genericModelSubstitutes(ResponseEntity.class)
        .forCodeGeneration(true)
        .genericModelSubstitutes(ResponseEntity.class)
        .directModelSubstitute(java.time.LocalDate.class, String.class)
        .directModelSubstitute(java.time.ZonedDateTime.class, Date.class)
        .directModelSubstitute(java.time.LocalDateTime.class, Date.class)
        .select()
        .paths(regex(DEFAULT_INCLUDE_PATTERN))
        .build();
    watch.stop();
    log.debug("Started Swagger in {} ms", Long.valueOf(watch.getTotalTimeMillis()));
    return docket;
  }
}
