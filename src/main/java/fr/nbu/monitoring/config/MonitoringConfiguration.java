package fr.nbu.monitoring.config;

import fr.nbu.monitoring.repository.ApplicationRepository;
import fr.nbu.monitoring.web.rest.StatusUpdater;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Configuration
public class MonitoringConfiguration {

  @Bean
  @ConditionalOnMissingBean
  public StatusUpdater statusUpdater(final ApplicationRepository applicationRepository) {
    final RestTemplate template = new RestTemplate();
    template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
    template.setErrorHandler(new DefaultResponseErrorHandler() {
      @Override
      protected boolean hasError(final HttpStatus statusCode) {
        return false;
      }
    });
    return new StatusUpdater(applicationRepository, template);
  }

  @Bean
  Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder() {
    final JavaTimeModule module = new JavaTimeModule();
    return new Jackson2ObjectMapperBuilder()
        .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .findModulesViaServiceLoader(true)
        .modulesToInstall(module);
  }
}
