package fr.nbu.monitoring.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;

import javax.inject.Inject;

import fr.nbu.monitoring.domain.Application;
import fr.nbu.monitoring.repository.ApplicationRepository;
import fr.nbu.monitoring.web.rest.dto.LoggerDTO;
import fr.nbu.monitoring.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * REST controller for managing Application.
 */
@RestController
@RequestMapping("/api")
public class ApplicationResource {

  private final Logger LOG = LoggerFactory.getLogger(ApplicationResource.class);

  @Inject
  private ApplicationRepository applicationRepository;

  @Inject
  private StatusUpdater statusUpdater;

  private final ObjectMapper mapper = new ObjectMapper();

  /**
   * POST  /api/applications -> Create a new application.
   */
  @RequestMapping(value = "/applications", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Application> createApplication(@RequestBody final Application application) throws URISyntaxException {
    LOG.debug("REST request to save Application : {}", application);

    if (applicationRepository.exist(application.getName())) {
      return ResponseEntity.badRequest().header("Failure", "A application with that name already exist").body(null);
    }
    // set booleans
    application.setMetrics(statusUpdater.queryMetrics(application) != null);
    application.setDump(statusUpdater.queryDump(application) != null);
    application.setBeans(statusUpdater.queryBeans(application) != null);
    application.setLogs(statusUpdater.queryLogs(application) != null);
    final String env = statusUpdater.queryEnv(application);
    application.setEnvironment(env != null);
    if (env != null) {
      try {
        final JsonNode rootNode = mapper.readTree(env);
        final JsonNode systemePropertiesNode = rootNode.get("systemProperties"); // Get the only element in the root node
        if (systemePropertiesNode != null) {
          final String catalinaBase = systemePropertiesNode.get("catalina.home").textValue();
          application.setCatalinaBase(catalinaBase);
        }
      } catch (final Exception e) {
        LOG.error("Unable to read json env", e);
      }
    }
    application.setStatusInfo(statusUpdater.queryStatusInfo(application));

    final Application savedApplication = applicationRepository.save(application);

    return ResponseEntity.created(new URI("/api/applications/" + savedApplication.getId()))
        .headers(HeaderUtil.createEntityCreationAlert("application", savedApplication.getId()))
        .body(savedApplication);
  }

  /**
   * GET  /api/applications -> get all the applications.
   */
  @RequestMapping(value = "/applications", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public Collection<Application> getAllApplications() {
    LOG.debug("REST request to get all Applications");

    return statusUpdater.updateStatusForAllApplications();
  }

  /**
   * GET  /api/applications/:id -> get the "id" application.
   */
  @RequestMapping(value = "/applications/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Application> getApplication(@PathVariable final String id) {
    LOG.debug("REST request to get Application : {}", id);
    final Application application = applicationRepository.findOne(id);

    if (application != null) {
      statusUpdater.updateStatus(application);
      return new ResponseEntity<>(application, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * GET  /api/applications/:id/health -> get health status of "id" application.
   */
  @RequestMapping(value = "/applications/{id}/health", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
  @Timed
  public ResponseEntity<String> getApplicationHealth(@PathVariable final String id) {
    LOG.debug("REST request to get health status of application : {}", id);
    final Application application = applicationRepository.findOne(id);

    if (application != null) {
      final String json = statusUpdater.queryHealth(application);
      return Optional.ofNullable(application)
          .map(app -> new ResponseEntity<>(json, HttpStatus.OK))
          .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * GET  /applications/:id/metrics -> get metrics status of "id" application.
   */
  @RequestMapping(value = "/applications/{id}/metrics", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
  @Timed
  public ResponseEntity<String> getApplicationMetrics(@PathVariable final String id) {
    LOG.debug("REST request to get metrics of application : {}", id);
    final Application application = applicationRepository.findOne(id);

    if (application != null && application.isMetrics()) {
      final String json = statusUpdater.queryMetrics(application);
      return Optional.ofNullable(json)
          .map(app -> new ResponseEntity<>(json, HttpStatus.OK))
          .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * GET  /api/applications/:id/dump -> get dump of "id" application.
   */
  @RequestMapping(value = "/applications/{id}/dump", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
  @Timed
  public ResponseEntity<String> getApplicationDump(@PathVariable final String id) {
    LOG.debug("REST request to get dump of application : {}", id);
    final Application application = applicationRepository.findOne(id);

    if (application != null && application.isDump()) {
      final String json = statusUpdater.queryDump(application);

      return Optional.ofNullable(json)
          .map(app -> new ResponseEntity<>(json, HttpStatus.OK))
          .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * GET  /api/applications/:id/env -> get env of "id" application.
   */
  @RequestMapping(value = "/applications/{id}/env", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
  @Timed
  public ResponseEntity<String> getApplicationEnv(@PathVariable final String id) {
    LOG.debug("REST request to get env of application : {}", id);
    final Application application = applicationRepository.findOne(id);

    if (application != null && application.isEnvironment()) {
      final String json = statusUpdater.queryEnv(application);

      return Optional.ofNullable(json)
          .map(app -> new ResponseEntity<>(json, HttpStatus.OK))
          .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * GET  /api/applications/:id/beans -> get beans of "id" application.
   */
  @RequestMapping(value = "/applications/{id}/beans", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
  @Timed
  public ResponseEntity<String> getApplicationBeans(@PathVariable final String id) {
    LOG.debug("REST request to get spring beans of application : {}", id);
    final Application application = applicationRepository.findOne(id);

    if (application != null && application.isBeans()) {
      final String json = statusUpdater.queryBeans(application);

      return Optional.ofNullable(json)
          .map(app -> new ResponseEntity<>(json, HttpStatus.OK))
          .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * GET  /api/applications/:id/logs -> get logs of "id" application.
   */
  @RequestMapping(value = "/applications/{id}/logs", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
  @Timed
  public ResponseEntity<String> getApplicationLogs(@PathVariable final String id) {
    LOG.debug("REST request to get spring logs of application : {}", id);
    final Application application = applicationRepository.findOne(id);

    if (application != null && application.isLogs()) {
      final String json = statusUpdater.queryLogs(application);

      return Optional.ofNullable(json)
          .map(app -> new ResponseEntity<>(json, HttpStatus.OK))
          .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * PUT /api/applications/:id -> delete the "id" application.
   *
   * @param jsonLogger
   */
  @RequestMapping(value = "/applications/{id}/logs", method = RequestMethod.PUT)
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @Timed
  public void changeLevel(@PathVariable final String id, @RequestBody final LoggerDTO jsonLogger) {
    LOG.debug("REST request to put spring logs of application : {}", id);
    final Application application = applicationRepository.findOne(id);

    if (application != null && application.isLogs()) {
      statusUpdater.updateLogs(application, jsonLogger);
    }
  }

  /**
   * DELETE  /api/applications/:id -> delete the "id" application.
   */
  @RequestMapping(value = "/applications/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Void> deleteApplication(@PathVariable final String id) {
    LOG.debug("REST request to delete Application : {}", id);

    applicationRepository.delete(id);
    return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("application", id.toString())).build();
  }

}
