package fr.nbu.monitoring.web.rest;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import fr.nbu.monitoring.domain.Application;
import fr.nbu.monitoring.domain.StatusInfo;
import fr.nbu.monitoring.repository.ApplicationRepository;
import fr.nbu.monitoring.web.rest.dto.LoggerDTO;

/**
 * The StatusUpdater is responsible for updatig the status of all or a single application querying the healthUrl.
 */
public class StatusUpdater {
  private static final Logger LOGGER = LoggerFactory.getLogger(StatusUpdater.class);

  private static final String HEALTH = "/health";

  private static final String METRICS = "/metrics/metrics";

  private static final String DUMP = "/dump";

  private static final String ENV = "/env";

  private static final String BEANS = "/beans";

  private static final String LOGS = "/api/logs";

  /**
   * Lifetime of status in ms. The status won't be updated as long the last status isn't expired.
   */
  private long statusLifetime = 10_000L;

  private final ApplicationRepository applicationRepository;

  private final RestTemplate restTemplate;

  public StatusUpdater(final ApplicationRepository applicationRepository, final RestTemplate restTemplate) {
    super();
    this.applicationRepository = applicationRepository;
    this.restTemplate = restTemplate;
  }

  public List<Application> updateStatusForAllApplications() {
    final List<Application> applications = applicationRepository.findAll();
    return applications.parallelStream().map(a -> updateStatus(a)).collect(Collectors.toList());
  }

  public Application updateStatus(final Application application) {
    boolean procedUpdate = true;

    if (application.getStatusInfo() != null) {
      final long now = System.currentTimeMillis();
      if (now - statusLifetime < application.getStatusInfo().getTimestamp()) {
        procedUpdate = false;
      }
    }
    if (procedUpdate) {
      final StatusInfo newStatus = queryStatusInfo(application);
      final Application newState = new Application(application, newStatus);
      return applicationRepository.save(newState);
    }
    return application;
  }

  public void setStatusLifetime(final long statusLifetime) {
    this.statusLifetime = statusLifetime;
  }

  StatusInfo queryStatusInfo(final Application application) {
    LOGGER.debug("Updating status for {}", application);

    final HttpEntity<String> entity = getHttpEntity(application);

    try {
      @SuppressWarnings("unchecked")
//      final ResponseEntity<Map<String, Object>> response = restTemplate.getForEntity(application.getRootUrl() + HEALTH, (Class<Map<String, Object>>) (Class<?>) Map.class);

      final ResponseEntity<Map<String, Object>> response = restTemplate.exchange(application.getRootUrl() + HEALTH, HttpMethod.GET, entity, (Class<Map<String, Object>>) (Class<?>) Map.class);

      LOGGER.debug("/health for {} responded with {}", application, response);

      if (response.hasBody() && response.getBody().get("status") instanceof String) {
        return StatusInfo.valueOf((String) response.getBody().get("status"), response.getBody());
      }
      else if (response.getStatusCode().is2xxSuccessful()) {
        return StatusInfo.ofUp(response.getBody());
      }
      else {
        return StatusInfo.ofDown(response.getBody());
      }

    } catch (final Exception ex) {
      LOGGER.debug("Couldn't retrieve status for {}", application);
      return StatusInfo.ofOffline(null);
    }
  }

  public String queryHealth(final Application application) {
    ResponseEntity<String> response = null;

    if (application.getRootUrl() != null) {
      LOGGER.debug("Query /health for {}", application);
      response = queryUrl(application, HEALTH);
      return response.getBody();
    }
    return null;
  }

  public String queryMetrics(final Application application) {
    ResponseEntity<String> response = null;

    if (application.getRootUrl() != null) {
      LOGGER.debug("Query /metrics for {}", application);
      response = queryUrl(application, METRICS);
      return response.getBody();
    }
    return null;
  }

  public String queryDump(final Application application) {
    ResponseEntity<String> response = null;

    if (application.getRootUrl() != null) {
      LOGGER.debug("Query /dump for {}", application);
      response = queryUrl(application, DUMP);
      return response.getBody();
    }
    return null;
  }

  public String queryEnv(final Application application) {
    ResponseEntity<String> response = null;

    if (application.getRootUrl() != null) {
      LOGGER.debug("Query /env for {}", application);
      response = queryUrl(application, ENV);
      return response.getBody();
    }
    return null;
  }

  public String queryBeans(final Application application) {
    ResponseEntity<String> response = null;

    if (application.getRootUrl() != null) {
      LOGGER.debug("Query /beans for {}", application);
      response = queryUrl(application, BEANS);
      return response.getBody();
    }
    return null;
  }

  public String queryLogs(final Application application) {
    ResponseEntity<String> response = null;

    if (application.getRootUrl() != null) {
      LOGGER.debug("Query /logs for {}", application);
      response = queryUrl(application, LOGS);
      return response.getBody();
    }
    return null;
  }

  public void updateLogs(final Application application, final LoggerDTO jsonLogger) {
    if (application.getRootUrl() != null) {
      LOGGER.debug("Put /logs for {}", application);
      final String url = application.getRootUrl() + LOGS;
      restTemplate.put(url, jsonLogger);
    }
  }

  private void setSessionCookie(final Application application, final ResponseEntity<String> response) {
    if (response.getHeaders() != null && response.getHeaders().get("Set-Cookie") != null) {
      application.setCookies(response.getHeaders().get("Set-Cookie"));
    }
  }

  private ResponseEntity<String> queryUrl(final Application application, final String url) {
    LOGGER.debug("Query url for {}", application.getRootUrl() + url);

    final HttpEntity<String> entity = getHttpEntity(application);

    try {
      final ResponseEntity<String> response = restTemplate.exchange(application.getRootUrl() + url, HttpMethod.GET, entity, String.class);
      setSessionCookie(application, response);

      LOGGER.debug("query for {} responded with {}", url, response);
      if (response.getStatusCode().equals(HttpStatus.OK)) {
        return response;
      }
    } catch (final Exception ex) {
      LOGGER.debug("Couldn't retrieve health status for {}", url);
    }
    return null;
  }

  private HttpEntity<String> getHttpEntity(Application application) {
    final HttpHeaders headers = new HttpHeaders();
    final List<String> cookies = application.getCookies();

    LOGGER.debug("Cookies {}", cookies);
    if (cookies != null) {
      LOGGER.debug("Set cookie {}", cookies);
      headers.set("Cookie", cookies.stream().collect(Collectors.joining(";")));
    }
    return new HttpEntity<>(headers);
  }

}
