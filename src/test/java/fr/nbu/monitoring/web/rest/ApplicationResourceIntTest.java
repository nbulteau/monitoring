package fr.nbu.monitoring.web.rest;

import static org.assertj.core.api.Assertions.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import fr.nbu.monitoring.Monitoring;
import fr.nbu.monitoring.domain.Application;
import fr.nbu.monitoring.repository.ApplicationRepository;

/**
 * Test class for the ApplicationResource REST controller.
 *
 * @see ApplicationResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Monitoring.class)
@WebAppConfiguration
@IntegrationTest
public class ApplicationResourceIntTest {

  private static final String DEFAULT_URL = "AAAAA";

  private static final String DEFAULT_NAME = "AAAAA";

  @Inject
  private ApplicationRepository applicationRepository;

  @Inject
  private StatusUpdater statusUpdater;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  private MockMvc restApplicationMockMvc;

  private Application application;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    final ApplicationResource applicationResource = new ApplicationResource();
    ReflectionTestUtils.setField(applicationResource, "applicationRepository", applicationRepository);
    ReflectionTestUtils.setField(applicationResource, "statusUpdater", statusUpdater);
    this.restApplicationMockMvc = MockMvcBuilders.standaloneSetup(applicationResource).setMessageConverters(jacksonMessageConverter).build();
  }

  @Before
  public void initTest() {
    application = new Application("ID", DEFAULT_NAME, DEFAULT_URL, null);
  }

  @Test
  public void getAllApplications() throws Exception {
    // Initialize the database
    applicationRepository.save(application);

    // Get all the applications
    restApplicationMockMvc.perform(get("/api/applications"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.[*].id").value(hasItem(application.getId())))
        .andExpect(jsonPath("$.[*].rootUrl").value(hasItem(DEFAULT_URL.toString())))
        .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
  }

  @Test
  public void getApplication() throws Exception {
    // Initialize the database
    applicationRepository.save(application);

    // Get the application
    restApplicationMockMvc.perform(get("/api/applications/{id}", application.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id").value(application.getId()))
        .andExpect(jsonPath("$.rootUrl").value(DEFAULT_URL.toString()))
        .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
  }

  @Test
  public void getNonExistingApplication() throws Exception {
    // Get the application
    restApplicationMockMvc.perform(get("/api/applications/{id}", "pouetpouet"))
        .andExpect(status().isNotFound());
  }

  @Test
  public void deleteApplication() throws Exception {
    // Initialize the database
    applicationRepository.save(application);

    final int databaseSizeBeforeDelete = applicationRepository.findAll().size();

    // Get the application
    restApplicationMockMvc.perform(delete("/api/applications/{id}", application.getId())
        .accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    final List<Application> applications = applicationRepository.findAll();
    assertThat(applications).hasSize(databaseSizeBeforeDelete - 1);
  }
}
